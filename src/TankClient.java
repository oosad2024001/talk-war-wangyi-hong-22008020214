import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import java.awt.event.*;
import java.util.ArrayList;

public class TankClient extends Frame {
	 public static final int GAME_WIDTH = 800; 
	 public static final int GAME_HEIGHT = 600;

	 int x=50,y=50;
	
	 Tank myTank = new Tank(300, 300,true,this,Tank.Direction.STOP);
	 List<Missile> missiles = new ArrayList<Missile>();
	 List<Explode> explodes=new ArrayList<Explode>();
	 List<Tank> tanks=new ArrayList<Tank>();
	 //Wall wall=new Wall(50,300,100,50,this);

	 Wall w1 = new Wall(100, 200, 20, 150, this),
		  w2 = new Wall(300, 100, 300, 20, this);
	 Blood b=new Blood();
	// Tank enemyTank = new Tank(100, 100,  this,false);
	 
	Image offScreenImage = null;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TankClient tc = new TankClient();
		tc.launchFrame();
		
		

	}
	
	public void update(Graphics g) {
		if(offScreenImage == null) {
			offScreenImage=this.createImage(GAME_WIDTH,GAME_HEIGHT);
		}
		Graphics gOffScreen = offScreenImage.getGraphics();
		Color c = gOffScreen.getColor(); 
		gOffScreen.setColor(Color.GREEN); 
		gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
		gOffScreen.setColor(c); 
		print(gOffScreen); 
		g.drawImage(offScreenImage, 0, 0, null);
	}
	public void paint(Graphics g) {
		g.drawString("missiles count: " + missiles.size(), 10, 50);
		g.drawString("explodes count: " + explodes.size(), 10,70);
		g.drawString("tanks count:"+tanks.size(),10,90);
		g.drawString("tank	life:"+myTank.getLife(),10,110);
		if(tanks.size() <= 0) {
			for(int i = 0; i < 5; i++) {
			tanks.add(new Tank(50 + 40 * (i + 1), 50,
			false, this,Tank.Direction.D));
			}
			}
		for(int i = 0; i < missiles.size(); i++) { 
			Missile m = missiles.get(i);
			//m.hitTank(enemyTank);
			m.hitTanks(tanks);
			m.collidesWithTank(myTank);
			m.hitWall(w1);
			m.hitWall(w2);
//			if(m!=null && m.isLive()) {
			m.draw(g);
//
//			}

			if(!m.isLive()) missiles.remove(m);
			
		
		}
		for(int i = 0; i < explodes.size(); i++) { 
			Explode e = explodes.get(i); 
			e.draw(g);
		}
		
//		 for(int i = 0 ;i < tanks.size();i++){
//	            tanks.get(i).draw(g);
//	        }
		  for (int i = 0; i < tanks.size(); i++) {
	            Tank t=tanks.get(i);
	            t.collidesWithWall(w1);
	            t.collidesWithWall(w2);
	            t.collidesWithTanks(tanks);
	            t.draw(g); 
	        }
		myTank.draw(g); 
		myTank.eat(b);
		myTank.collidesWithWall(w1);
		myTank.collidesWithWall(w2);

		myTank.collidesWithTanks(tanks);
		w1.draw(g);
		w2.draw(g);
		b.draw(g);
	
		 
	}
	
	public void launchFrame() {
//		for (int i = 0; i < 10; i++) {
//            tanks.add(new Tank(50+40*(i+1),50,this,false));
//        }
		this.setLocation(300, 100);
		this.setSize(GAME_WIDTH, GAME_HEIGHT);
		this.setTitle("TankWar");
		addWindowListener(new WindowAdapter() {
		    public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
            }
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
			public void windowActivated(WindowEvent e) {
	             super.windowActivated(e);
	        }
			public void windowDeactivated(WindowEvent e) {
                setTitle("TankWaar");
            }
		});
		this.setResizable(false);
		this.setBackground(Color.GREEN);
		this.addKeyListener(new KeyMonitor()); 
		setVisible(true);
		
		new Thread(new PaintThread()).start();
		  for (int i = 0; i < 5; i++) {
	            tanks.add(new Tank(50+40*(i+1),50,false,this,Tank.Direction.STOP));
	        }
	}
    private class PaintThread implements Runnable{
    	public void run() {
    		while(true) {
    			repaint();
    			try {
    				Thread.sleep(50);
    			}catch(InterruptedException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    } 
    private class KeyMonitor extends KeyAdapter{
    	public void keyPressed(KeyEvent e) {
    		myTank.KeyPressed(e);
    	}
    	public void keyReleased (KeyEvent e) {
    		myTank.keyRelease (e);
    	}
    }
    
  
}
